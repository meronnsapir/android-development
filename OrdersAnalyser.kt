import java.time.LocalDateTime
import java.math.BigDecimal
import java.time.DayOfWeek

class OrdersAnalyzer {
    data class Order(val orderId: Int, val orderDate: LocalDateTime, val orderLines: List<OrderLine>)
    data class OrderLine(val productId: Int, val productName: String, val quantity: Int, val productPrice: BigDecimal)

    fun totalDailySales(orders: List<Order>): Map<DayOfWeek, Int> {
        val quantitiesPerDayMap = mutableMapOf<DayOfWeek, Int>()
        for (order in orders) {
            for (orderline in order.orderLines) {
                val dayCount = quantitiesPerDayMap[order.creationDate.dayOfWeek] ?: 0
                quantitiesPerDayMap.put(
                    order.creationDate.dayOfWeek,
                    dayCount + orderline.quantity
                )
            }
        }
        return quantitiesPerDayMap
    }
}